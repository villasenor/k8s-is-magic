# k8s-is-magic

Code from K8s is Magic!

## What's included?

### Demo Code
Code that ran during the demo - scaling up and deploying workloads.

### Persistent Volume Code Example
Deploy a workload that uses a 1GB disk

### GPU Code Example
Deploy a workload that utilizes an NVIDIA GPU

### Replicas and Load Balancing Code Example
Deploy a workload that utilizes multiple copies of itself that are load balanced